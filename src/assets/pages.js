export const pages = [
  { title: 'Home', url: '/', component: 'StartPage' },
  { title: 'Players', url: '/players-setup', component: 'PlayersSetup' },
  { title: 'Game card', url: '/game-card', component: 'GameCard' },
]
